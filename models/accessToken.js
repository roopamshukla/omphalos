const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const accessToken = new Schema({
    userId: {
        "type": "ObjectId",
        "required": true,
        "unique": true
    },
    token: {
        "type": String,
        "required": true,
        "default": null
    }
});

module.exports = mongoose.model('accessToken', accessToken);