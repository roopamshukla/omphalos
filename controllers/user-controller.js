const users = require('../models/user');
const bcrypt = require('bcrypt');
const errorCtrl = require('../modules/error-handler');
const accessTokenCtrl = require('../modules/jwt-handler');
// var jwt = require('jsonwebtoken');
// const config = require('../config/config').config;
async function addNewUser(ctx) {
    try {
        let newUserData = new users(ctx.request.body);
        await newUserData.validate();
        newUserData.password = await bcrypt.hash(newUserData.password, 10);
        let a = await users.create(newUserData);
        a = a.toObject();
        delete a["password"];
        delete a["_id"];
        ctx.body = a;
    } catch (e) {
        errorCtrl.sendError(e, ctx);
    }
}

async function userLogin(ctx) {
    try {
        let newUserData = new users(ctx.request.body);
        await newUserData.validate();
        let user = await users.findOne({
            "username": ctx.request.body.username
        })
        let passwordResult = await bcrypt.compare(ctx.request.body.password, user.password);
        if (passwordResult) {
            await accessTokenCtrl.setAccessToken(ctx, user.id)
            ctx.body = {
                message: "Login successful"
            };
        } else {
            errorCtrl.sendError({}, ctx, "Login failed. Please check the credentials", 401);
        }
    } catch (e) {
        errorCtrl.sendError(e, ctx);
    }
}

async function getAllUsers(ctx) {
    try {
        let result = await users.find({}).select("username -_id");
        ctx.body = result;
    } catch (e) {
        errorCtrl.sendError(e, ctx);
    }
}

async function userLogout(ctx) {
    try {
        let r = await accessTokenCtrl.deleteAccessToken(ctx)
        ctx.body = {
            message: "Logout successful"
        };
    } catch (e) {
        errorCtrl.sendError(e, ctx);
    }
}

module.exports = {
    addNewUser,
    userLogin,
    userLogout,
    getAllUsers
}