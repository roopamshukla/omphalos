let config = {
    "appKeys": ["pen", "pineapple", "apple", "pen"],
    "jwtSecret": "in-the-end-it-doesnt-even-matter",
    "moongodb": "mongodb://localhost/ptah"
};
if (process.env.NODE_ENV == "production") {
    config.appKeys = ["pen", "pineapple", "apple", "pen"]
    config.jwtSecret = "in-the-end-it-doesnt-even-matter"
    config.moongodb = `mongodb+srv://roopamshukla:${process.env.DB_PASS}@poseidon-rzlr8.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
}
if (process.env.NODE_ENV == "preproduction") {}
if (process.env.NODE_ENV == "development") {
    config.appKeys = ["pen", "pineapple", "apple", "pen"]
    config.jwtSecret = "in-the-end-it-doesnt-even-matter"
    config.moongodb = "mongodb://localhost/ptah"
}

if (!process.env.NODE_ENV) {
    console.error("ENV VARIABLE NOT SET");
}

if (process.env.NODE_ENV == "production") {
    if (!process.env.DB_PASS) {
        console.log("DB PASS NOT SET");
    }
    if (!process.env.DB_NAME) {
        console.log("DB NAME NOT SET");
    }
}


module.exports = {
    config
}