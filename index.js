const Koa = require('koa');
const app = new Koa();
const cors = require('@koa/cors');
const bodyparser = require('koa-body');
const config = require('./config/config').config;
const authCtrl = require('./modules/authenticator');
const errorCtrl = require('./modules/error-handler');

const mongoose = require('mongoose');
//all api routes
const apiRoutes = require('./routes/route-handler');

mongoose.connect(config.moongodb, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', (err) => {
    console.info("Mongo connection error : ", err);
});
db.once('open', () => {
    console.info("Successfully connected to mongo");
});

app.keys = config.appKeys;

app.use(cors({
    credentials: true
}));
app.use(bodyparser());

app.use(async (ctx, next) => {
    if (ctx.path == "/api/user/login") {
        await next();
    } else {
        try {
            await authCtrl.checkAuth(ctx);
            await next();
        } catch (e) {
            ctx.cookies.set("token", null, {
                "signed": true,
                "maxAge": 3600000
            })
            errorCtrl.sendError(e, ctx, "", 401)
        }
    }
})

app.use(apiRoutes.routes())
    .use(apiRoutes.allowedMethods());

console.log("NODE_ENV value : ", app.env);
app.listen(3000);