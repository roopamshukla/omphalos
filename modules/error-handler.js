function formatError(msg = "", status = 500, details = {}) {
    let e = new Error();
    e.message = msg;
    e.status = status;
    if (process.env.NODE_ENV !== "production") {
        e.details = details.toString();
    }
    return e;
}

function sendError(e = {}, ctx, customMessage, customCode) {
    e.status = e.statusCode || e.status || customCode || 400;
    e.message = e.message || customMessage || "someting went wrong at user";
    ctx.status = e.status;
    console.error(e);
    ctx.body = formatError(e.message, e.status, e);
}

module.exports = {
    sendError
}