const accessToken = require('../models/accessToken');
const jwt = require('jsonwebtoken');
const config = require('../config/config').config;
const errorCtrl = require('../modules/error-handler');
const mongoose = require('mongoose');

async function setAccessToken(ctx, userId) {
    let token = jwt.sign({
        userId: userId,
    }, config.jwtSecret, {
        expiresIn: '1h',
        issuer: "ptah"
    });

    try {
        let newAccessToken = new accessToken({
            userId: mongoose.Types.ObjectId(userId),
            token: token
        });
        await newAccessToken.validate();
        // newAccessToken = newAccessToken.toObject();

        ctx.cookies.set("token", token, {
            "signed": true,
            "maxAge": 3600000
        })

        await accessToken.updateOne({
            userId: mongoose.Types.ObjectId(userId)
        }, {
            $set: {
                userId: mongoose.Types.ObjectId(userId),
                token: token
            }
        }, {
            upsert: true
        })
    } catch (e) {
        ctx.cookies.set("token", null, {
            "signed": true,
            "maxAge": 3600000
        })
        throw e;
    }
}

async function checkAccessToken(token, userId) {
    try {
        let tokenResult = await accessToken.findOne({
            userId: mongoose.Types.ObjectId(userId),
            token: token
        })

        return tokenResult;
    } catch (e) {
        throw e
    }
}

function verifyToken(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.jwtSecret, function (err, decoded) {
            if (!err && decoded) {
                resolve(decoded);
            } else {
                console.error("JWT decode error : ", err);
                reject(new Error("Unable to decode token"))
            }
        });
    });
}

async function deleteAccessToken(ctx) {
    try {
        let deleteTokenResult = await accessToken.deleteMany({
            userId: mongoose.Types.ObjectId(ctx.accessToken.userId),
        })
        ctx.cookies.set("token", null, {
            "signed": true,
            "maxAge": 3600000
        })
        return deleteTokenResult;
    } catch (e) {
        throw e
    }
}

module.exports = {
    setAccessToken,
    checkAccessToken,
    verifyToken,
    deleteAccessToken
}