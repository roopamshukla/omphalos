const accessTokenCtrl = require('../modules/jwt-handler');

async function checkAuth(ctx) {
    let token = ctx.cookies.get("token", {
        signed: true
    })
    if (token) {
        try {
            var decodedToken = await accessTokenCtrl.verifyToken(token)
        } catch (e) {
            console.error(e);
            throw e;
        }
        let aTresult = await accessTokenCtrl.checkAccessToken(token, decodedToken.userId);
        if (aTresult) {
            ctx.accessToken = aTresult;
            return true;
        } else {
            throw new Error("Unauthorized")
        }
    } else {
        console.error("Token not found in cookie")
        throw new Error("Token not found")
    }
}

module.exports = {
    checkAuth
}