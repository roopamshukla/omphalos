const Router = require('koa-router');
const userController = require('../controllers/user-controller');

const userRouter = new Router({
    prefix: '/user'
})

userRouter
    .post('/', userController.addNewUser)
    .post('/login', userController.userLogin)
    .delete('/logout', userController.userLogout)
    .get('/', userController.getAllUsers);

module.exports = userRouter;