const Router = require('koa-router');
const userRoutes = require('./user-routes');

const apiRoutes = new Router({
    prefix: '/api'
})

apiRoutes.use('', userRoutes.routes(), userRoutes.allowedMethods());

module.exports = apiRoutes;